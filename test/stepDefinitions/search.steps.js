import { defineSupportCode } from 'cucumber'
import {expect} from 'Chai';
import homePage from '../pageobjects/home.page'
import confirmUbicationModal from '../pageobjects/confirmUbicationModal.page';
import searchResults from '../pageobjects/searchResults.page';

defineSupportCode(function({ Given, When, Then }) {

    Given (/^I am on the home page$/, function(){
        homePage.open()
    })

    When(/^I select "([^"]*)" as the department$/, function(department) {
        homePage.clickCityAddress()
        homePage.setCity(department)
    })

    When(/^I enter "([^"]*)" as the address$/, function (address) {
        homePage.setAddress(address)
        homePage.clickBuscarButton()
        confirmUbicationModal.clickConfirmButton()
    })

    Then(/^I am taken to the restaurants list$/, function () {
       expect(searchResults.getTitleEspected()).to.be.include('locales')
    })

    Then(/^I can see restaurants in the list$/, function () {
       expect(searchResults.isARestaurantPresent() > 0)        
        browser.pause(9000)      
     })

})
