import { format } from "path";
import {world} from '../world';

class restaurantResult {
    get foodSearchField()       {return browser.element('#menuSearch')}
    get foodSearchResult()      {return browser.elements('#menu > div > section:nth-child(13) > ul > li > div > h4 > mark')}
    get allFoods()              {return browser.elements('.productName')}
    get foodRows()              {return browser.elements('li[title = "Editar" ]')}
    get nameFoods()             {return browser.elements('.nameWrapper')}
    get foodAmountDropdown()    {return browser.element('div.productQuantity.dropdown')}
    get foodAmounts()           {return browser.elements(`div.productQuantity.dropdown > select > option[value = '${this.value}']`)}
    get subtotalPrice()         {return browser.element('div[data-auto = "shopdetails_cart_subtotal"]')}
    get totalPrice()            {return browser.element('.price.total-price')}


    createArrayWorld(){
        world.foodAddedManually = []
        world.foodAddedDisplayed = []
    }

    foodSearch(food) {
        this.allFoods.waitForVisible(9000)
        let res = this.allFoods.value.length
        for(let i = 0; i < res; i++){
            let resFoodName = this.allFoods.value[i].getText();
            if(resFoodName === food){
                this.allFoods.value[i].click();
               world.foodAddedManually.push(resFoodName)
               console.log(world.foodAddedManually)
                break;
            }

        }
    }

    setFoodAmount(foodAdded , amount) {
        for(let menus of this.foodRows.value){
            let food = browser.elementIdElement(menus.value.ELEMENT , this.nameFoods.selector).getText()
            console.log('Food: ' + food)
            if(food === foodAdded){
                console.log('Food: ' + food + '----> FoodAdded: ' + foodAdded)
                browser.elementIdElement(menus.value.ELEMENT , this.foodAmountDropdown.selector).click()
                this.value = amount
                browser.elementIdElement(menus.value.ELEMENT , this.foodAmounts.selector).click()
                browser.pause(9000)
            }
        }
    }

    getallFoodDisplayed() {
        console.log('Obtengo las comidas desplegadas en el cart')
        for(let i = 0; i < this.nameFoods.value.length; i++){
            let comida = this.nameFoods.value[i].getText()
            console.log(comida)
            world.foodAddedDisplayed.push(comida)
            console.log('Array 2 = ' + world.foodAddedDisplayed) 
        }
    }

    getSubtotalPrice() {
        this.subtotalPrice.waitForVisible(9000)
        let subprice = this.subtotalPrice.getText()
        console.log('The subtotal price in the web is: ' + subprice)
        return subprice
    }

    getTotalPrice() {
        this.subtotalPrice.waitForVisible(9000)
        let totPrice = this.totalPrice.getText()
        console.log('The total price in the web is: ' + totPrice)
        return totPrice
    }

}

export default new restaurantResult()